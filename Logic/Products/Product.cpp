#include "Product.hpp"

using namespace Products;

Product::Product(const std::string& name)
{
    m_name = name;
}

std::string Product::GetName() const
{
    return m_name;
}
