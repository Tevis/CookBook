#include "ProductDb.hpp"

using namespace Products;

ProductDb* ProductDb::s_instance = nullptr;

ProductDb* ProductDb::Get()
{
    if(!s_instance)
        s_instance = new ProductDb;

    return s_instance;
}


ProductDb::ProductDb()
{
    m_database.push_back(Product("product1"));
    m_database.push_back(Product("product2"));
    m_database.push_back(Product("product3"));
}

std::list<Product> ProductDb::GetProductList() const
{
    return m_database;
}

void ProductDb::AddProduct(const Product& product)
{
    m_database.push_back(product);
}
