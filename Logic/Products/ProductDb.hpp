#ifndef PRODUCTDB_H
#define PRODUCTDB_H

#include "Product.hpp"

#include <list>

namespace Products {

class ProductDb {

public:

    static ProductDb* Get();

    ProductDb();

    std::list<Product> GetProductList() const;

    void AddProduct(const Product& product);

private:
    static ProductDb* s_instance;

    std::list<Product> m_database;




};

}

#endif // PRODUCTDB_H
