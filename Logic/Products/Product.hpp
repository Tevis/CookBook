#ifndef PRODUCT_H
#define PRODUCT_H

#include <string>

namespace Products {

class Product
{
public:
    Product(const std::string& name);

    std::string GetName() const;

private:
    std::string m_name;
};


}

#endif // PRODUCT_H
