#include "mainwindow.h"
#include "ui_mainwindow.h"



#include <QPushButton>
#include <QTabBar>

#include "Logic/Products/ProductDb.hpp"



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    std::map<GUI::PageNavigation::EPageIndex, QPushButton*> naviButtons;

    naviButtons[GUI::PageNavigation::EPageIndex::Index] = ui->indexPageButton;
    naviButtons[GUI::PageNavigation::EPageIndex::Products] = ui->productPageButton;
    naviButtons[GUI::PageNavigation::EPageIndex::Recipe] = ui->recipePageButton;

    m_navigation = new GUI::PageNavigation(ui->pageNavigator, naviButtons);
    m_productsPage = new GUI::ProductsPage(ui->productsTable);
    m_recipePage = new GUI::RecipePage(ui->recipeTabs);


    m_recipePage->NewRecipe();

}

MainWindow::~MainWindow()
{
    delete ui;
}

