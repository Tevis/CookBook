#include "PageNavigation.hpp"

#include <QTabBar>

using namespace GUI;

PageNavigation::PageNavigation(QTabWidget* navigationTabWidget, const std::map<EPageIndex, QPushButton*>& navigationButtons) :
    m_navigationTabWidget(navigationTabWidget),
    m_navigationButtons(navigationButtons)
{
    m_navigationTabWidget->tabBar()->hide();

    connect(m_navigationButtons[EPageIndex::Index], SIGNAL(clicked()), this, SLOT(GoToIndexPage()));
    connect(m_navigationButtons[EPageIndex::Products], SIGNAL(clicked()), this, SLOT(GoToProductsPage()));
    connect(m_navigationButtons[EPageIndex::Recipe], SIGNAL(clicked()), this, SLOT(GoToRecipePage()));

};

void PageNavigation::GoToIndexPage()
{
    GoToPage(EPageIndex::Index);
}

void PageNavigation::GoToProductsPage()
{
    GoToPage(EPageIndex::Products);
}

void PageNavigation::GoToRecipePage()
{
    GoToPage(EPageIndex::Recipe);
}

void PageNavigation::GoToPage(EPageIndex index)
{
    m_navigationTabWidget->setCurrentIndex(static_cast<int>(index));
}
