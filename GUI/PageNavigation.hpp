#ifndef PAGENAVIGATION_H
#define PAGENAVIGATION_H

#include <QTabWidget>
#include <QPushButton>
#include <QObject>
#include <map>

namespace GUI {


class PageNavigation : public QObject
{
    Q_OBJECT

public:
    enum class EPageIndex
    {
        Index = 0,
        Recipe = 1,
        Products = 2,
    };

    PageNavigation(QTabWidget* navigationTabWidget, const std::map<EPageIndex, QPushButton*>& navigationButtons);

private slots:
    void GoToIndexPage();
    void GoToProductsPage();
    void GoToRecipePage();

private:




    void GoToPage(EPageIndex index);

    QTabWidget* m_navigationTabWidget;
    std::map<EPageIndex, QPushButton*> m_navigationButtons;
};

}



#endif // PAGENAVIGATION_H
