#ifndef PRODUCTSPAGE_H
#define PRODUCTSPAGE_H

#include <QObject>
#include <QTableWidget>

namespace GUI {

class ProductsPage : public QObject
{
    Q_OBJECT

public:
    ProductsPage(QTableWidget* productsTable);

    void Refresh();

private slots:
    void HandleCellClick(int row, int column);

private:
    QTableWidget* m_productsTable;

    void AddCreationRow();

};
}

#endif // PRODUCTSPAGE_H
