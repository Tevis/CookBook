#include "ProductsPage.hpp"

using namespace GUI;

ProductsPage::ProductsPage(QTableWidget* productsTable) : m_productsTable(productsTable)
{
    Refresh();
};

void ProductsPage::Refresh()
{
    m_productsTable->setColumnCount(8); //TODO: column count based on unit count
    //TODO: insert rows based on product count

    AddCreationRow();
}

void ProductsPage::HandleCellClick(int row, int column)
{
    if(column == m_productsTable->columnCount() - 1) //Last column, add or removal
    {
        if(row == m_productsTable->rowCount() - 1) //Last row, add entry
        {
            m_productsTable->insertRow(m_productsTable->rowCount());

            //TODO: Add entry to database
        }
        else //Middle row, remove entry
        {
            m_productsTable->removeRow(row);

            //TODO: Remove entry from databse
        }
    }
}

void ProductsPage::AddCreationRow()
{
    auto creationRow = m_productsTable->rowCount();

    m_productsTable->insertRow(creationRow);

    connect(m_productsTable, SIGNAL(cellClicked(int, int)), this, SLOT(HandleCellClick(int, int)));
}


