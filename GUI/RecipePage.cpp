#include "RecipePage.hpp"

using namespace GUI;

RecipePage::RecipePage(QTabWidget* recipeTabs) : m_recipeTabs(recipeTabs)
{


}

void RecipePage::NewRecipe()
{
    auto widget = new QWidget();

    m_recipeTabs->insertTab(m_recipeTabs->count(), widget, "NewRecipe");

    m_tabCtrls.push_back(new RecipeTab(widget));

}
