#ifndef RECIPETAB_H
#define RECIPETAB_H

#include <QWidget>

namespace GUI {


class RecipeTab : public QObject
{
    Q_OBJECT

public:
    RecipeTab(QWidget* tab);

private:
    QWidget* m_widget;

};

}


#endif // RECIPETAB_H
