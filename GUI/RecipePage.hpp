#ifndef RECIPEPAGE_H
#define RECIPEPAGE_H

#include <QTabWidget>
#include <QObject>
#include "RecipeTab.hpp"

namespace GUI {


class RecipePage : public QObject
{
    Q_OBJECT

public:
    RecipePage(QTabWidget* recipeTabs);

    void NewRecipe();

private:

    QTabWidget* m_recipeTabs;

    std::list<RecipeTab*> m_tabCtrls;

};

}


#endif // RECIPEPAGE_H
