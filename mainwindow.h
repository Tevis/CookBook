#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "GUI/PageNavigation.hpp"
#include "GUI/ProductsPage/ProductsPage.hpp"
#include "GUI/RecipePage.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    GUI::PageNavigation* m_navigation;
    GUI::ProductsPage* m_productsPage;
    GUI::RecipePage* m_recipePage;
};
#endif // MAINWINDOW_H
